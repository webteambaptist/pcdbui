﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PCDB_Rewrite.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using PCDB_Rewrite.Helper;
using static PCDB_Rewrite.Models.DropsDowns;

namespace PCDB_Rewrite.Controllers
{
    public class HomeController : Controller
    {
        private readonly pcdbContext _config;
        private readonly string _user;
        private readonly string userName;

        DateTime start = DateTime.Parse("01/01/" + DateTime.Now.Year);
        DateTime end = DateTime.Parse("12/31/" + DateTime.Now.Year);

        private readonly int _beaches;
        private readonly int _downtown;
        private readonly int _nassau;
        private readonly int _phs;

        private readonly string _adAdminGroupName;
        private readonly string _adUserGroupName;
        private readonly string _domain;

        private readonly string[] letters = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
            "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z", "ALL"};

        public HomeController(pcdbContext c, IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            _config = c;
            _user = httpContextAccessor.HttpContext.User.Identity.Name;
            userName = Authentication.cleanUsername(_user);

            _beaches = Convert.ToInt32(configuration["AppSettings:Locations:Beaches"]);
            _downtown = Convert.ToInt32(configuration["AppSettings:Locations:Downtown"]);
            _nassau = Convert.ToInt32(configuration["AppSettings:Locations:Nassau"]);
            _phs = Convert.ToInt32(configuration["AppSettings:Locations:PHS"]);

            _adAdminGroupName = configuration["AppSettings:ADAdminGroupName"];
            _adUserGroupName = configuration["AppSettings:ADUserGroupName"];
            _domain = configuration["AppSettings:Domain"];
        }

        public IActionResult Index()
        {
            ViewBag.Letters = letters;
            
            //Check if logged in user is a part of the admin group
            var adAdminAccess = ADAccess.GetADAccess(_domain, _user, _adAdminGroupName);

            // If logged in user is not part of the admin group, check the User Group
            if (!adAdminAccess)
            {
                var adUserAccess = ADAccess.GetADAccess(_domain, _user, _adUserGroupName);

                if (adUserAccess)
                {
                    ViewBag.Access = "User";
                    return View();
                }

                return RedirectToAction("NotAuthorized", "Home");
            }

            ViewBag.Access = "Admin";
            return View();
        }

        public IActionResult NotAuthorized()
        {
            return View();
        }

        public IActionResult RecipientFamilyAdd()
        {
            ViewBag.Titles = _config.TblTitles.ToList();
            ViewBag.Suffixes = _config.TblSuffixes.ToList();

            var Relationships = new List<Relationships>();

            var relationshipsList = _config.TblRelationshipTypes.Where(x=>x.DisplayRelationshipType == "Y").ToList();

            foreach (var relationship in relationshipsList)
            {
                var r = new Relationships
                {
                    Id = relationship.RelationshipTypeIdPk.ToString(),
                    Relationship = relationship.RelationshipType.Trim()
                };
                Relationships.Add(r);
            }

            ViewBag.Relationships = Relationships;
            ViewBag.Letters = letters;

            //Check if logged in user is a part of the admin group
            var adAdminAccess = ADAccess.GetADAccess(_domain, _user, _adAdminGroupName);

            // If logged in user is not part of the admin group, check the User Group
            if (!adAdminAccess)
            {
                var adUserAccess = ADAccess.GetADAccess(_domain, _user, _adUserGroupName);

                if (adUserAccess)
                {
                    ViewBag.Access = "User";
                    return View();
                }

                return RedirectToAction("NotAuthorized", "Home");
            }

            ViewBag.Access = "Admin";
            return View();
        }

        public IActionResult RecipientNSPAdd()
        {
            ViewBag.Titles = _config.TblTitles.ToList();
            ViewBag.Suffixes = _config.TblSuffixes.ToList();
            
            ViewBag.Letters = letters;
            
             //Check if logged in user is a part of the admin group
            var adAdminAccess = ADAccess.GetADAccess(_domain, _user, _adAdminGroupName);

            // If logged in user is not part of the admin group, check the User Group
            if (!adAdminAccess)
            {
                var adUserAccess = ADAccess.GetADAccess(_domain, _user, _adUserGroupName);

                if (adUserAccess)
                {
                    ViewBag.Access = "User";
                    return View();
                }

                return RedirectToAction("NotAuthorized", "Home");
            }

            ViewBag.Access = "Admin";
            return View();
        }

        public IActionResult RecipientView(string recipientID)
        {
            var recipient = _config.TblRecipients.FirstOrDefault(x => x.RecipientIdPk == recipientID);

            var result = new RecipientView
            {
                RecipientID = recipient.RecipientIdPk.Trim(),
                Name = recipient.RecipientTitle.Trim() + " " + recipient.RecipientFirstName.Trim() + " " +
                       recipient.RecipientLastName.Trim(),
                Comments = recipient.RecipientComments
            };

            //Check if logged in user is a part of the admin group
            var adAdminAccess = ADAccess.GetADAccess(_domain, _user, _adAdminGroupName);

            // If logged in user is not part of the admin group, check the User Group
            if (!adAdminAccess)
            {
                var adUserAccess = ADAccess.GetADAccess(_domain, _user, _adUserGroupName);

                if (adUserAccess)
                {
                    ViewBag.Access = "User";
                    return View(result);
                }

                return RedirectToAction("NotAuthorized", "Home");
            }

            ViewBag.Access = "Admin";
            return View(result);
        }

        public IActionResult RecipientFamilyViewEdit(string recipientID)
        {
            var recipient = _config.TblRecipients.FirstOrDefault(x => x.RecipientIdPk == recipientID);

            var result = new RecipientViewFamily
            {
                RecipientID = recipient.RecipientIdPk.Trim(),
                Title = recipient.RecipientTitle.Trim(),
                FirstName = recipient.RecipientFirstName.Trim(),
                MiddleInitial = recipient.RecipientMi.Trim(),
                LastName = recipient.RecipientLastName.Trim(),
                Suffix = recipient.RecipientSuffix.Trim(),
                Relationship = _config.TblRelationshipTypes
                    .Where(x => x.RelationshipTypeIdPk == recipient.RelationshipTypeIdFk)
                    .Select(x => x.RelationshipType).FirstOrDefault().Trim(),
                Comments = recipient.RecipientComments
            };

            var relatedToKey = _config.TblRecipients.FirstOrDefault(x => x.RecipientIdPk == recipient.RecipientIdFk);
            result.RelatedTo = relatedToKey.RecipientTitle.Trim() + " " + relatedToKey.RecipientFirstName.Trim() +
                               " " + relatedToKey.RecipientLastName.Trim() + " (" + relatedToKey.RecipientIdPk.Trim() + ")";

            ViewBag.Titles = _config.TblTitles.ToList();
            ViewBag.Suffixes = _config.TblSuffixes.ToList();
           // ViewBag.Relationships = _config.TblRelationshipTypes.Where(x => x.DisplayRelationshipType == "Y").ToList();

           var relationshipsList = _config.TblRelationshipTypes.Where(x => x.DisplayRelationshipType == "Y").ToList();

           var Relationships = relationshipsList.Select(relationship => new Relationships {Id = relationship.RelationshipTypeIdPk.ToString(), Relationship = relationship.RelationshipType.Trim()}).ToList();

           ViewBag.Relationships = Relationships;

            //Check if logged in user is a part of the admin group
            var adAdminAccess = ADAccess.GetADAccess(_domain, _user, _adAdminGroupName);

            // If logged in user is not part of the admin group, check the User Group
            if (!adAdminAccess)
            {
                var adUserAccess = ADAccess.GetADAccess(_domain, _user, _adUserGroupName);

                if (!adUserAccess)
                {
                    return RedirectToAction("NotAuthorized", "Home");
                }

                ViewBag.Access = "User";
                return View(result);
            }

            ViewBag.Access = "Admin";
            return View(result);
        }

        
        public IActionResult CourtesyList(string recipientID)
        {
            var recipient = _config.TblRecipients.FirstOrDefault(x => x.RecipientIdPk == recipientID);

            var result = new CourtesyList
            {
                RecipientID = recipient.RecipientIdPk.Trim(),
                Beaches = "$" + Math.Round(Convert.ToDecimal(_config.TblRecipientsCourtesies.Where(x =>
                    x.RecipientIdFk == recipient.RecipientIdPk && x.HospitalIdFk == _beaches &&
                    x.DateGiven > start && x.DateGiven < end).Sum(x => x.ValueGiven)), 2),
                Downtown = "$" + Math.Round(Convert.ToDecimal(_config.TblRecipientsCourtesies.Where(x =>
                    x.RecipientIdFk == recipient.RecipientIdPk && x.HospitalIdFk == _downtown &&
                    x.DateGiven > start && x.DateGiven < end).Sum(x => x.ValueGiven)), 2),
                Nassau = "$" + Math.Round(Convert.ToDecimal(_config.TblRecipientsCourtesies.Where(x =>
                    x.RecipientIdFk == recipient.RecipientIdPk && x.HospitalIdFk == _nassau &&
                    x.DateGiven > start && x.DateGiven < end).Sum(x => x.ValueGiven)), 2),
                PHS = "$" + Math.Round(Convert.ToDecimal(_config.TblRecipientsCourtesies.Where(x =>
                    x.RecipientIdFk == recipient.RecipientIdPk && x.HospitalIdFk == _phs && x.DateGiven > start &&
                    x.DateGiven < end).Sum(x => x.ValueGiven)), 2)
            };


            if (recipient.RelationshipTypeIdFk != 0)
            {
                var relationshipID = _config.TblRelationshipTypes
                    .Where(x => x.RelationshipTypeIdPk == recipient.RelationshipTypeIdFk)
                    .Select(x => x.RelationshipType).FirstOrDefault();

                result.Name = recipient.RecipientTitle.Trim() + " " + recipient.RecipientFirstName.Trim() + " " +
                              recipient.RecipientLastName.Trim() + " (" + recipient.RecipientIdPk.Trim() + ") [" + relationshipID + "]";

                var relatedToKey = _config.TblRecipients.FirstOrDefault(x => x.RecipientIdPk == recipient.RecipientIdFk);
                
                result.RelatedTo = relatedToKey.RecipientTitle.Trim() + " " + relatedToKey.RecipientFirstName.Trim() +
                                   " " + relatedToKey.RecipientLastName.Trim() + " (" + relatedToKey.RecipientIdPk.Trim() + ")";
            }
            else
            {
                result.Name = recipient.RecipientTitle.Trim() + " " + recipient.RecipientFirstName.Trim() +
                              " " + recipient.RecipientLastName.Trim() + " (" + recipient.RecipientIdPk.Trim() + ")";
                result.RelatedTo = "self";
            }

            ViewBag.Years = _config.TblYears.ToList();

            //Check if logged in user is a part of the admin group
            var adAdminAccess = ADAccess.GetADAccess(_domain, _user, _adAdminGroupName);

            // If logged in user is not part of the admin group, check the User Group
            if (!adAdminAccess)
            {
                var adUserAccess = ADAccess.GetADAccess(_domain, _user, _adUserGroupName);

                if (!adUserAccess)
                {
                    return RedirectToAction("NotAuthorized", "Home");
                }

                ViewBag.Access = "User";
                return View(result);
            }

            ViewBag.Access = "Admin";
            return View(result);
        }

        public IActionResult CourtesyViewEdit(string id, string year)
        {
            var courtesyId = Convert.ToInt32(id);
            var courtesy = _config.TblRecipientsCourtesies.FirstOrDefault(x => x.RecipientCourtesyIdPk == courtesyId);

            var results = new CourtesyDetails {Id = courtesyId};

            var recipient = _config.TblRecipients
                .FirstOrDefault(x => x.RecipientIdPk == courtesy.RecipientIdFk);

            var relatedToKey = _config.TblRecipients.FirstOrDefault(x => x.RecipientIdPk == recipient.RecipientIdFk);

            if (recipient.RelationshipTypeIdFk != 0)
            {
                var relationshipID = _config.TblRelationshipTypes
                    .Where(x => x.RelationshipTypeIdPk == recipient.RelationshipTypeIdFk)
                    .Select(x => x.RelationshipType).FirstOrDefault();

                results.Recipient = recipient.RecipientTitle.Trim() + " " + recipient.RecipientFirstName.Trim() + " " +
                                    recipient.RecipientLastName.Trim() + " (" + recipient.RecipientIdPk.Trim() + ") [" + relationshipID + "]";
                results.RelatedTo = relatedToKey.RecipientTitle.Trim() + " " + relatedToKey.RecipientFirstName.Trim() +
                                    " " + relatedToKey.RecipientLastName.Trim() + " (" + relatedToKey.RecipientIdPk.Trim() + ")";
            }
            else
            {
                results.Recipient = recipient.RecipientTitle.Trim() + " " + recipient.RecipientFirstName.Trim() +
                                    " " + recipient.RecipientLastName.Trim() + " (" + recipient.RecipientIdPk.Trim() + ")";

                results.RelatedTo = "self";
            }

            results.RecipientId = recipient.RecipientIdPk.Trim();
            
            results.DateGiven = Convert.ToDateTime(courtesy.DateGiven);

            var selectedBhEntity = _config.TblHospitals.FirstOrDefault(x => x.HospitalIdPk == courtesy.HospitalIdFk);
            
            results.BHEntity = selectedBhEntity.HospitalIdPk;

            var givenBy = _config.TblSmts.FirstOrDefault(x => x.SmtidPk == courtesy.SmtidFk);

            results.GivenBy = givenBy.SmtidPk;

            results.GivenByStatus = givenBy.Smtactive;

            if (results.GivenByStatus == "I")
            {
                var inactiveGivenBy = string.IsNullOrEmpty(givenBy.Smtmi)
                    ? givenBy.SmtfirstName.Trim() + " " + givenBy.SmtlastName.Trim()
                    : givenBy.SmtfirstName.Trim() + " " + givenBy.Smtmi.Trim() + " " + givenBy.SmtlastName.Trim();

                results.InactiveGivenBy = inactiveGivenBy;
            }

            var selectedCourtesyType = _config.TblCourtesyTypes
                .FirstOrDefault(x => x.CourtesyTypeIdPk == courtesy.CourtesyTypeIdFk);

            results.CourtesyType = selectedCourtesyType.CourtesyTypeIdPk;

            results.Value = Math.Round(Convert.ToDecimal(courtesy.ValueGiven), 2);

            results.Description = courtesy.Description;

            //Build BH Entites List
            results._BHEntites = new List<BHEntites>();
            
            var bhEntitesList = _config.TblHospitals.ToList();

            foreach (var b in bhEntitesList.Select(bhEntity => new BHEntites
            {
                Id = bhEntity.HospitalIdPk.ToString(),
                BHEntity = bhEntity.HospitalName.Trim()
            }))
            {
                results._BHEntites.Add(b);
            }

            ViewBag.BHEntities = results._BHEntites;

            //Build Given Name List
            results._GivenByNames = new List<GivenByNames>();

            var givenByList = _config.TblSmts.ToList();

            foreach (var g in from item in givenByList where !string.IsNullOrEmpty(item.SmtfirstName) && !string.IsNullOrEmpty(item.SmtlastName) select new GivenByNames
            {
                Id = item.SmtidPk.ToString(),
                Name = string.IsNullOrEmpty(item.Smtmi)
                    ? item.SmtfirstName.Trim() + " " + item.SmtlastName.Trim()
                    : item.SmtfirstName.Trim() + " " + item.Smtmi.Trim() + " " + item.SmtlastName.Trim()
            })
            {
                results._GivenByNames.Add(g);
            }
            
            ViewBag.GivenBy = results._GivenByNames;

            //Build Courtest Type List
            results._CourtesyTypes = new List<CourtesyTypes>();
            
            var courtesyByList = _config.TblCourtesyTypes.ToList();

            foreach (var cType in courtesyByList.Select(courtesyType => new CourtesyTypes
            {
                Id = courtesyType.CourtesyTypeIdPk.ToString(),
                CourtesyType = courtesyType.CourtesyType.Trim()
            }))
            {
                results._CourtesyTypes.Add(cType);
            }

            ViewBag.CourtesyTypes = results._CourtesyTypes;
            
            ViewBag.Year = year;

            //Check if logged in user is a part of the admin group
            var adAdminAccess = ADAccess.GetADAccess(_domain, _user, _adAdminGroupName);

            // If logged in user is not part of the admin group, check the User Group
            if (!adAdminAccess)
            {
                var adUserAccess = ADAccess.GetADAccess(_domain, _user, _adUserGroupName);

                if (adUserAccess)
                {
                    ViewBag.Access = "User";
                    return View(results);
                }

                return RedirectToAction("NotAuthorized", "Home");
            }

            ViewBag.Access = "Admin";
            return View(results);
        }

        public IActionResult CourtesyAdd(string recipientId)
        {
            var recipient = _config.TblRecipients.FirstOrDefault(x => x.RecipientIdPk == recipientId);

            var result = new CourtesyAdd
            {
                Id = recipient.RecipientIdPk.Trim(),
            };


            if (recipient.RelationshipTypeIdFk != 0)
            {
                var relationshipID = _config.TblRelationshipTypes
                    .Where(x => x.RelationshipTypeIdPk == recipient.RelationshipTypeIdFk)
                    .Select(x => x.RelationshipType).FirstOrDefault();

                result.Recipient = recipient.RecipientTitle.Trim() + " " + recipient.RecipientFirstName.Trim() + " " +
                                   recipient.RecipientLastName.Trim() + " (" + recipient.RecipientIdPk.Trim() + ") [" + relationshipID + "]";

                var relatedToKey = _config.TblRecipients.FirstOrDefault(x => x.RecipientIdPk == recipient.RecipientIdFk);

                result.RelatedTo = relatedToKey.RecipientTitle.Trim() + " " + relatedToKey.RecipientFirstName.Trim() +
                                   " " + relatedToKey.RecipientLastName.Trim() + " (" + relatedToKey.RecipientIdPk.Trim() + ")";
            }
            else
            {
                result.Recipient = recipient.RecipientTitle.Trim() + " " + recipient.RecipientFirstName.Trim() +
                                   " " + recipient.RecipientLastName.Trim() + " (" + recipient.RecipientIdPk.Trim() + ")";
                result.RelatedTo = "self";
            }

            result._BHEntites = new List<BHEntites>();

            var bhEntitesList = _config.TblHospitals.ToList();

            foreach (var b in bhEntitesList.Select(bhEntity => new BHEntites
            {
                Id = bhEntity.HospitalIdPk.ToString(),
                BHEntity = bhEntity.HospitalName.Trim()
            }))
            {
                result._BHEntites.Add(b);
            }

            ViewBag.BHEntities = result._BHEntites;

            result._GivenByNames = new List<GivenByNames>();

            var givenByList = _config.TblSmts.ToList();

            foreach (var g in from item in givenByList where !string.IsNullOrEmpty(item.SmtfirstName) && !string.IsNullOrEmpty(item.SmtlastName) select new GivenByNames
            {
                Id = item.SmtidPk.ToString(),
                Name = string.IsNullOrEmpty(item.Smtmi)
                    ? item.SmtfirstName.Trim() + " " + item.SmtlastName.Trim()
                    : item.SmtfirstName.Trim() + " " + item.Smtmi.Trim() + " " + item.SmtlastName.Trim()
            })
            {
                result._GivenByNames.Add(g);
            }

            ViewBag.GivenBy = result._GivenByNames;

            result._CourtesyTypes = new List<CourtesyTypes>();

            var courtesyByList = _config.TblCourtesyTypes.ToList();

            foreach (var cType in courtesyByList.Select(courtesyType => new CourtesyTypes
            {
                Id = courtesyType.CourtesyTypeIdPk.ToString(),
                CourtesyType = courtesyType.CourtesyType.Trim()
            }))
            {
                result._CourtesyTypes.Add(cType);
            }

            ViewBag.CourtesyTypes = result._CourtesyTypes;

            //Check if logged in user is a part of the admin group
            var adAdminAccess = ADAccess.GetADAccess(_domain, _user, _adAdminGroupName);

            // If logged in user is not part of the admin group, check the User Group
            if (!adAdminAccess)
            {
                var adUserAccess = ADAccess.GetADAccess(_domain, _user, _adUserGroupName);

                if (adUserAccess)
                {
                    ViewBag.Access = "User";
                    return View(result);
                }

                return RedirectToAction("NotAuthorized", "Home");
            }

            ViewBag.Access = "Admin";
            return View(result);
        }

        //public IActionResult Privacy()
        //{
        //    return View();
        //}

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(string error)
        {
            ViewBag.ErrorMessage = error;
            return View();
        }
    }
}
