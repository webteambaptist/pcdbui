﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PCDB_Rewrite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PCDB_Rewrite.Helper;

namespace PCDB_Rewrite.Controllers
{
    public class AdminController : Controller
    {
        private readonly pcdbContext _config;
        private readonly string _user;
        private readonly string userName;

        DateTime start = DateTime.Parse("01/01/" + DateTime.Now.Year);
        DateTime end = DateTime.Parse("12/31/" + DateTime.Now.Year);

        private readonly string _adAdminGroupName;
        private readonly string _domain;

        public AdminController(pcdbContext c, IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            _config = c;
            _user = httpContextAccessor.HttpContext.User.Identity.Name;
            //userName = Authentication.cleanUsername(_user);

            _adAdminGroupName = configuration["AppSettings:ADAdminGroupName"];
            _domain = configuration["AppSettings:Domain"];
        }
        
        public IActionResult Index()
        {
            var adAdminAccess = ADAccess.GetADAccess(_domain, _user, _adAdminGroupName);

            // If logged in user is not part of the admin group, display Not Authorized page.
            if (!adAdminAccess)
            {
                    return RedirectToAction("NotAuthorized", "Home");
            }
            else
            {
                ViewBag.Access = "Admin";
                return View();
            }
        }

        public IActionResult Hospitals()
        {
            var adAdminAccess = ADAccess.GetADAccess(_domain, _user, _adAdminGroupName);

            // If logged in user is not part of the admin group, display Not Authorized page.
            if (!adAdminAccess) return RedirectToAction("NotAuthorized", "Home");
            ViewBag.Access = "Admin";
            return View();
        }

        public IActionResult CourtesyTypes()
        {
            var adAdminAccess = ADAccess.GetADAccess(_domain, _user, _adAdminGroupName);

            // If logged in user is not part of the admin group, display Not Authorized page.
            if (!adAdminAccess) return RedirectToAction("NotAuthorized", "Home");
            ViewBag.Access = "Admin";
            return View();

        }
        public IActionResult SMTs()
        {
            
            var adAdminAccess = ADAccess.GetADAccess(_domain, _user, _adAdminGroupName);

            // If logged in user is not part of the admin group, display Not Authorized page.
            if (!adAdminAccess) return RedirectToAction("NotAuthorized", "Home");
            ViewBag.Access = "Admin";
            return View();

        }
        public IActionResult RelationshipTypes()
        {
            var adAdminAccess = ADAccess.GetADAccess(_domain, _user, _adAdminGroupName);

            // If logged in user is not part of the admin group, display Not Authorized page.
            if (!adAdminAccess) return RedirectToAction("NotAuthorized", "Home");
            ViewBag.Access = "Admin";
            return View();

        }
    }
}
