﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using PCDB_Rewrite.Helper;
using PCDB_Rewrite.Models;
using Microsoft.Extensions.Configuration;
using NLog;

namespace PCDB_Rewrite.Controllers
{
    public class SubmitController : Controller
    {
        private readonly pcdbContext _config;
        private readonly string _user;
        private readonly string userName;
        private readonly string _nonPhysicianPrefix;
        private readonly int _nonPhysicianValue;
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();


        public SubmitController(pcdbContext c, IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            _config = c;
            _user = httpContextAccessor.HttpContext.User.Identity.Name;

            _nonPhysicianPrefix = configuration["AppSettings:NonPhysicianPrefix"];
            _nonPhysicianValue = Convert.ToInt32(configuration["AppSettings:NonPhysicianValue"]);
            // userName = Authentication.cleanUsername(_user);

            var config = new NLog.Config.LoggingConfiguration();
            var orFile = new NLog.Targets.FileTarget("logfile") { FileName = $"PCDBSubmissions{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, orFile);
            LogManager.Configuration = config;
        }
        
        [HttpPost]
        public IActionResult UpdateFamilyRecipient(IFormCollection collection)
        {
            try
            {
                var recipient = _config.TblRecipients.FirstOrDefault(x => x.RecipientIdPk == collection["RecipientID"].ToString());
                recipient.RecipientTitle = collection["Title"].ToString();
                recipient.RecipientFirstName = collection["firstName"].ToString();
                recipient.RecipientMi = collection["middleInitial"].ToString();
                recipient.RecipientLastName = collection["lastName"].ToString();
                recipient.RecipientSuffix = collection["suffixType"].ToString();
                recipient.RelationshipTypeIdFk = _config.TblRelationshipTypes.Where(x => x.RelationshipType == collection["relationshipType"].ToString()).Select(x => x.RelationshipTypeIdPk).FirstOrDefault();
                recipient.RecipientComments = collection["comments"].ToString();
                recipient.LastModifiedDate = DateTime.Now;
                recipient.LastModifiedUser = _user;

                _config.SaveChanges();
                
                return RedirectToAction("RecipientFamilyViewEdit", "Home",
                    new {recipientID = collection["recipientID"].ToString() });
            }
            catch (Exception e)
            {
                //Log update issue.
                _logger.Error("There was an issue Updating Family Recipient " + e.Message);
                return RedirectToAction("Error", "Home", new { error = "There was an issue Updating Family Recipient :: " + e.Message });
            }
        }

        [HttpPost]
        public IActionResult UpdateCourtesyDetails(IFormCollection collection)
        {
            try
            {
                var id = Convert.ToInt32(collection["Id"]);
                
                var bhEntity = collection["bhEntity"].ToString().Split(',');
                var hospitalIdFk = Convert.ToInt16(bhEntity[1]);

                var givenBy = collection["givenBy"].ToString().Split(',');

                int smtidFk;

                smtidFk = Convert.ToInt32(!string.IsNullOrEmpty(collection["inactiveGivenBy"]) ? collection["givenBy"].ToString() : givenBy[1]);

                var courtesyType = collection["CourtesyType"].ToString().Split(',');
                var courtesyTypeIdFk = Convert.ToInt16(courtesyType[1]);
                
                var c = _config.TblRecipientsCourtesies.FirstOrDefault(x => x.RecipientCourtesyIdPk == id);
                c.CourtesyTypeIdFk = courtesyTypeIdFk;
                c.HospitalIdFk = hospitalIdFk;
                c.DateGiven = Convert.ToDateTime(collection["dateGiven"].ToString());
                c.ValueGiven = Math.Round(Convert.ToDecimal(collection["value"].ToString()), 2);
                c.SmtidFk = smtidFk;

                c.Description = collection["description"].ToString();
                c.LastModifiedDate = DateTime.Now;
                c.LastModifiedUser = _user;

                _config.SaveChanges();
                
                return RedirectToAction("CourtesyList", "Home",
                    new { recipientID = collection["recipientId"].ToString() });
            }
            catch (Exception e)
            {
                //Log update issue.
                _logger.Error("There was an issue Updating Courtesy Details " + e.Message);
                return RedirectToAction("Error", "Home", new { error = "There was an issue Updating Courtesy Details :: " + e.Message });
            }
        }

        [HttpPost]
        public IActionResult AddNewCourtesy(IFormCollection collection)
        {
            try
            {
                var newCourtesy = new TblRecipientsCourtesy
                {
                    RecipientIdFk = collection["Id"].ToString(),
                    CourtesyTypeIdFk = Convert.ToInt16(collection["courtesyType"].ToString()),
                    HospitalIdFk = Convert.ToInt16(collection["bhEntity"].ToString()),
                    DateGiven = Convert.ToDateTime(collection["dateGiven"].ToString()),
                    ValueGiven = Convert.ToDecimal(collection["value"].ToString()),
                    SmtidFk = Convert.ToInt32(collection["givenBy"].ToString()),
                    Description = collection["description"].ToString(),
                    LastModifiedUser = _user,
                    LastModifiedDate = DateTime.Now,
                    RecordCreatedDate = DateTime.Now
                };

                _config.TblRecipientsCourtesies.Add(newCourtesy);

                _config.SaveChanges();

                return RedirectToAction("CourtesyList", "Home",
                    new { recipientID = collection["Id"].ToString() });
            }
            catch (Exception e)
            {
                //Log update issue.
                _logger.Error("There was an issue Inserting New Courtesy " + e.Message);

                return RedirectToAction("Error", "Home", new { error = "There was an issue Inserting New Courtesy :: " + e.Message });
            }
        }

        [HttpPost]
        public IActionResult AddNewFamilyRecipient(IFormCollection collection)
        {
            try
            {
                var fr = new TblRecipient();

                var id = collection["relatedToID"].ToString();
                var newRecipientId = "";

                var recipientCount = _config.TblRecipients.Count(x => x.RecipientIdPk.StartsWith(id));

                newRecipientId = id + "-" + (recipientCount);

                fr.RecipientIdPk = newRecipientId;
                fr.RecipientTitle = collection["titleType"].ToString();
                fr.RecipientFirstName = collection["firstName"].ToString();
                fr.RecipientMi = collection["middleInitial"].ToString();
                fr.RecipientLastName = collection["lastName"].ToString();
                fr.RecipientSuffix = collection["suffixType"].ToString();
                fr.RecipientComments = collection["description"].ToString();
                fr.RecipientIdFk = id;
                fr.RelationshipTypeIdFk = Convert.ToInt16(collection["relationshipType"].ToString());
                fr.LastModifiedUser = _user;
                fr.LastModifiedDate = DateTime.Now;
                fr.RecordCreatedDate = DateTime.Now;
                
                _config.TblRecipients.Add(fr);
                _config.SaveChanges();
                
                return RedirectToAction("RecipientFamilyAdd", "Home");
            }
            catch (Exception e)
            {
                //Log update issue.
                _logger.Error("There was an issue insert new Family Recipient " + e.Message);

                return RedirectToAction("Error", "Home", new { error = "There was an issue insert new Family Recipient :: " + e.Message });
            }
        }

        [HttpPost]
        public IActionResult AddNewNonPhysician(IFormCollection collection)
        {
            try
            {
                var np = new TblRecipient
                {
                    RecipientTitle = collection["titleType"].ToString(),
                    RecipientFirstName = collection["firstName"].ToString(),
                    RecipientMi = collection["middleInitial"].ToString(),
                    RecipientLastName = collection["lastName"].ToString(),
                    RecipientSuffix = collection["suffixType"].ToString(),
                    RecipientComments = collection["description"].ToString(),
                    LastModifiedUser = _user,
                    LastModifiedDate = DateTime.Now,
                    RecordCreatedDate = DateTime.Now,
                    RelationshipTypeIdFk = 14,
                    RecipientIdFk = "0"
                };


                //Generate Recipient ID PK
                
                var numGenerated = _config.TblRecipients.Count(x => x.RelationshipTypeIdFk == 14);
                var n = numGenerated + _nonPhysicianValue;

                var newRecipientId = _nonPhysicianPrefix + n;
                
                np.RecipientIdPk = newRecipientId;

                _config.TblRecipients.Add(np);
                _config.SaveChanges();

                return RedirectToAction("RecipientNSPAdd", "Home");
            }
            catch (Exception e)
            {
                //Log update issue.
                _logger.Error("There was an issue Inserting New Non-Physician " + e.Message);
                return RedirectToAction("Error", "Home", new { error = "There was an issue Inserting New Non-Physician :: " + e.Message });
            }
        }
        
        [HttpPost]
        public IActionResult AddNewHospital(IFormCollection collection)
        {
            try
            {
                var h = new TblHospital
                {
                    HospitalName = collection["hospitalName"].ToString()
                };

                _config.TblHospitals.Add(h);
                _config.SaveChanges();

                return RedirectToAction("Hospitals", "Admin");
            }
            catch (Exception e)
            {
                //Log update issue.
                _logger.Error("There was an issue Inserting New Hospital " + e.Message);
                return RedirectToAction("Error", "Home", new { error = "There was an issue Inserting New Hospital :: " + e.Message });
            }
        }

        [HttpPost]
        public IActionResult AddNewCourtesyType(IFormCollection collection)
        {
            try
            {
                var ct = new TblCourtesyType()
                {
                    CourtesyType = collection["courtesyType"].ToString()
                };

                _config.TblCourtesyTypes.Add(ct);
                _config.SaveChanges();

                return RedirectToAction("CourtesyTypes", "Admin");
            }
            catch (Exception e)
            {
                //Log update issue.
                _logger.Error("There was an issue Inserting New Courtesy Types" + e.Message);
               return RedirectToAction("Error", "Home", new { error = "There was an issue Inserting New Courtesy Types :: " + e.Message });
            }
        }

        [HttpPost]
        public IActionResult ToggleSMTStatus(string id)
        {
            var smtID = Convert.ToInt32(id);

            try
            {
                var smt = _config.TblSmts.FirstOrDefault(x => x.SmtidPk == smtID);

                smt.Smtactive = smt.Smtactive switch
                {
                    "A" => "I",
                    "I" => "A",
                    _ => smt.Smtactive
                };

                _config.SaveChanges();

                return Ok();
            }
            catch (Exception e)
            {
                //Log update issue.
                _logger.Error("There was an issue Updating the SMT's Status " + e.Message);
                return BadRequest();
            }
        }

        [HttpPost]
        public IActionResult AddNewSMT(IFormCollection collection)
        {
            try
            {
                var s = new TblSmt()
                {
                    SmtfirstName = collection["firstName"].ToString(), 
                    Smtmi = collection["middleInitial"].ToString(),
                    SmtlastName = collection["lastName"].ToString(),
                    Smtactive = "A"
                };

                _config.TblSmts.Add(s);
                _config.SaveChanges();

                return RedirectToAction("SMTs", "Admin");
            }
            catch (Exception e)
            {
                //Log update issue.
                _logger.Error("There was an issue Inserting New SMT " + e.Message);
                return RedirectToAction("Error", "Home", new { error = "There was an issue Inserting New SMT :: " + e.Message });
            }
        }

        [HttpPost]
        public IActionResult ToggleDisplayValue(string id)
        {
            var relationshipTypeId = Convert.ToInt32(id);

            try
            {
                var rt = _config.TblRelationshipTypes.FirstOrDefault(x => x.RelationshipTypeIdPk == relationshipTypeId);

                rt.DisplayRelationshipType = rt.DisplayRelationshipType switch
                {
                    "Y" => "N",
                    "N" => "Y",
                    _ => rt.DisplayRelationshipType
                };

                _config.SaveChanges();

                return Ok();
            }
            catch (Exception e)
            {
                //Log update issue.
                _logger.Error("There was an issue Updating the Display Value for Relationship Type" + e.Message);
                return BadRequest();
            }
        }

        [HttpPost]
        public IActionResult AddNewRelationshipType(IFormCollection collection)
        {
            try
            {
                var r = new TblRelationshipType()
                {
                    RelationshipType = collection["relationshipType"].ToString(),
                    DisplayRelationshipType = "Y"
                };

                _config.TblRelationshipTypes.Add(r);
                _config.SaveChanges();

                return RedirectToAction("RelationshipTypes", "Admin");
            }
            catch (Exception e)
            {
                //Log update issue.
                _logger.Error("There was an issue Inerting New Relationship Type" + e.Message);
                return RedirectToAction("Error", "Home", new {error = "There was an issue Inerting New Relationship Type :: " + e.Message });
            }
        }
    }
}
