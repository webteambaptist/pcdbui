﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using PCDB_Rewrite.Models;

namespace PCDB_Rewrite.Controllers
{
    public class QueryController : Controller
    {
        private readonly pcdbContext _config;
        DateTime _start = DateTime.Parse("01/01/" + DateTime.Now.Year);
        DateTime _end = DateTime.Parse("12/31/" + DateTime.Now.Year);

        private readonly int _beaches;
        private readonly int _downtown;
        private readonly int _nassau;
        private readonly int _phs;

        public QueryController(pcdbContext c, IConfiguration configuration)
        {
            _config = c;
            _beaches = Convert.ToInt32(configuration["AppSettings:Locations:Beaches"]);
            _downtown = Convert.ToInt32(configuration["AppSettings:Locations:Downtown"]);
            _nassau = Convert.ToInt32(configuration["AppSettings:Locations:Nassau"]);
            _phs = Convert.ToInt32(configuration["AppSettings:Locations:PHS"]);
        }

        [HttpGet]
        public IActionResult RecipientsSearch(string searchValue, string searchType)
        {
            var jsonData = "";
            try
            {
                var recipients = new List<TblRecipient>();
                var results = new List<RecipientsSearch>();

                switch (searchType)
                {
                    case "All Names":
                        recipients = _config.TblRecipients.OrderBy(x => x.RecipientLastName).ToList();
                        break;
                    case "Last Name Letter":
                        recipients = _config.TblRecipients.Where(x => x.RecipientLastName.StartsWith(searchValue))
                            .OrderBy(x => x.RecipientLastName).ToList();
                        break;
                }

                foreach (var recipient in recipients)
                {
                    var result = new RecipientsSearch
                    {
                        RecipientID = recipient.RecipientIdPk.Trim(),
                        Beaches = Convert.ToDecimal(_config.TblRecipientsCourtesies.Where(x =>
                            x.RecipientIdFk == recipient.RecipientIdPk && x.HospitalIdFk == _beaches &&
                            x.DateGiven > _start && x.DateGiven < _end).Sum(x => x.ValueGiven)),
                        Downtown = Convert.ToDecimal(_config.TblRecipientsCourtesies.Where(x =>
                            x.RecipientIdFk == recipient.RecipientIdPk && x.HospitalIdFk == _downtown &&
                            x.DateGiven > _start && x.DateGiven < _end).Sum(x => x.ValueGiven)),
                        Nassau = Convert.ToDecimal(_config.TblRecipientsCourtesies.Where(x =>
                            x.RecipientIdFk == recipient.RecipientIdPk && x.HospitalIdFk == _nassau &&
                            x.DateGiven > _start && x.DateGiven < _end).Sum(x => x.ValueGiven)),
                        PHS = Convert.ToDecimal(_config.TblRecipientsCourtesies.Where(x =>
                            x.RecipientIdFk == recipient.RecipientIdPk && x.HospitalIdFk == _phs &&
                            x.DateGiven > _start &&
                            x.DateGiven < _end).Sum(x => x.ValueGiven))
                    };

                    if (recipient.RelationshipTypeIdFk != 0)
                    {
                        result.RelationshipID = _config.TblRelationshipTypes
                            .Where(x => x.RelationshipTypeIdPk == recipient.RelationshipTypeIdFk)
                            .Select(x => x.RelationshipType).FirstOrDefault();

                        result.Name = recipient.RecipientTitle.Trim() + " " + recipient.RecipientFirstName.Trim() +
                                      " " +
                                      recipient.RecipientLastName.Trim() + " (" + recipient.RecipientIdPk.Trim() +
                                      ") [" + result.RelationshipID + "]";
                    }
                    else
                    {
                        result.Name = recipient.RecipientTitle.Trim() + " " + recipient.RecipientFirstName.Trim() +
                                      " " + recipient.RecipientLastName.Trim() + " (" + recipient.RecipientIdPk.Trim() +
                                      ")";
                    }

                    results.Add(result);
                }

                jsonData = JsonConvert.SerializeObject(results);
            }
            catch (Exception)
            {
                jsonData = "";
                //log exception
            }

            var jsonResult = Json(new {data = jsonData});
            return jsonResult;
        }

        [HttpGet]
        public IActionResult RecipientsSearchForFamilyRecipients(string searchValue, string searchType)
        {
            var jsonData = "";
            try
            {
                var recipients = new List<TblRecipient>();
                var results = new List<RecipientsSearch>();

                switch (searchType)
                {
                    case "All Names":
                        recipients = _config.TblRecipients.OrderBy(x => x.RecipientLastName).ToList();
                        break;
                    case "Last Name Letter":
                        recipients = _config.TblRecipients.Where(x => x.RecipientLastName.StartsWith(searchValue))
                            .OrderBy(x => x.RecipientLastName).ToList();
                        break;
                }

                foreach (var recipient in recipients)
                {
                    var result = new RecipientsSearch
                    {
                        RecipientID = recipient.RecipientIdPk.Trim()
                    };

                    if (recipient.RelationshipTypeIdFk != 0)
                    {
                        result.RelationshipID = _config.TblRelationshipTypes
                            .Where(x => x.RelationshipTypeIdPk == recipient.RelationshipTypeIdFk)
                            .Select(x => x.RelationshipType).FirstOrDefault();

                        result.Name = recipient.RecipientTitle.Trim() + " " + recipient.RecipientFirstName.Trim() +
                                      " " +
                                      recipient.RecipientLastName.Trim() + " (" + recipient.RecipientIdPk.Trim() +
                                      ") [" + result.RelationshipID + "]";
                    }
                    else
                    {
                        result.Name = recipient.RecipientTitle.Trim() + " " + recipient.RecipientFirstName.Trim() +
                                      " " + recipient.RecipientLastName.Trim() + " (" + recipient.RecipientIdPk.Trim() +
                                      ")";
                    }

                    results.Add(result);
                }

                jsonData = JsonConvert.SerializeObject(results);
            }
            catch (Exception)
            {
                jsonData = "";
                //log exception
            }

            var jsonResult = Json(new {data = jsonData});
            return jsonResult;
        }

        [HttpGet]
        public IActionResult CourtesyListSearch(string recipientId, string selectedYear)
        {
            var jsonData = "";

            var courtesyStart = DateTime.Parse("01/01/" + selectedYear);
            var courtesyEnd = DateTime.Parse("12/31/" + selectedYear);
            try
            {
                var courtesies = new List<TblRecipientsCourtesy>();
                var results = new List<CourtesyTableValues>();

                courtesies = _config.TblRecipientsCourtesies.Where(x =>
                        x.RecipientIdFk == recipientId && x.DateGiven > courtesyStart && x.DateGiven <= courtesyEnd)
                    .ToList();

                foreach (var courtesy in courtesies)
                {
                    var c = new CourtesyTableValues
                    {
                        Id = courtesy.RecipientCourtesyIdPk,
                        RecipientId = recipientId,
                        Date = Convert.ToDateTime(courtesy.DateGiven).ToShortDateString(),
                        BHEntity = _config.TblHospitals.Where(x => x.HospitalIdPk == courtesy.HospitalIdFk)
                            .Select(x => x.HospitalName).FirstOrDefault()
                    };
                    var givenBy = _config.TblSmts.FirstOrDefault(x => x.SmtidPk == courtesy.SmtidFk);

                    c.GivenBy = string.IsNullOrEmpty(givenBy.Smtmi)
                        ? givenBy.SmtfirstName.Trim() + " " + givenBy.SmtlastName.Trim()
                        : givenBy.SmtfirstName.Trim() + " " + givenBy.Smtmi.Trim() + " " + givenBy.SmtlastName.Trim();

                    c.CourtesyType = _config.TblCourtesyTypes
                        .Where(x => x.CourtesyTypeIdPk == courtesy.CourtesyTypeIdFk).Select(x => x.CourtesyType)
                        .FirstOrDefault();
                    c.Value = Math.Round(Convert.ToDecimal(courtesy.ValueGiven), 2);

                    var desc = courtesy.Description.Split('-');
                    c.Description = desc[0];

                    results.Add(c);
                }

                jsonData = JsonConvert.SerializeObject(results);
            }
            catch (Exception)
            {
                jsonData = "";
                // log exception
            }

            var jsonResult = Json(new {data = jsonData});
            return jsonResult;
        }

        [HttpGet]
        public IActionResult GetCourtesyTotals(string recipientId, string selectedYear)
        {
            var jsonData = "";

            var courtesyStart = DateTime.Parse("01/01/" + selectedYear);
            var courtesyEnd = DateTime.Parse("12/31/" + selectedYear);

            try
            {
                var c = new CourtesyTotal
                {
                    Beaches = "$" + Math.Round(Convert.ToDecimal(_config.TblRecipientsCourtesies.Where(x =>
                        x.RecipientIdFk == recipientId && x.HospitalIdFk == _beaches &&
                        x.DateGiven > courtesyStart && x.DateGiven <= courtesyEnd).Sum(x => x.ValueGiven)), 2),
                    Downtown = "$" + Math.Round(Convert.ToDecimal(_config.TblRecipientsCourtesies.Where(x =>
                        x.RecipientIdFk == recipientId && x.HospitalIdFk == _downtown &&
                        x.DateGiven > courtesyStart && x.DateGiven <= courtesyEnd).Sum(x => x.ValueGiven)), 2),
                    Nassau = "$" + Math.Round(Convert.ToDecimal(_config.TblRecipientsCourtesies.Where(x =>
                        x.RecipientIdFk == recipientId && x.HospitalIdFk == _nassau &&
                        x.DateGiven > courtesyStart && x.DateGiven <= courtesyEnd).Sum(x => x.ValueGiven)), 2),
                    PHS = "$" + Math.Round(Convert.ToDecimal(_config.TblRecipientsCourtesies.Where(x =>
                        x.RecipientIdFk == recipientId && x.HospitalIdFk == _phs &&
                        x.DateGiven > courtesyStart && x.DateGiven <= courtesyEnd).Sum(x => x.ValueGiven)), 2)
                };

                jsonData = JsonConvert.SerializeObject(c);
            }
            catch (Exception e)
            {
                jsonData = "";
                // log exception
            }

            var jsonResult = Json(new {data = jsonData});
            return jsonResult;
        }

        [HttpGet]
        public IActionResult GetProviderName(string recipientId)
        {
            var jsonData = "";
            var recipient = new FamilyMemberRelatedTo();

            try
            {
                var r = _config.TblRecipients.FirstOrDefault(x => x.RecipientIdPk == recipientId);

                if (r.RelationshipTypeIdFk != 0)
                {
                    var relationshipID = _config.TblRelationshipTypes
                        .Where(x => x.RelationshipTypeIdPk == r.RelationshipTypeIdFk)
                        .Select(x => x.RelationshipType).FirstOrDefault();

                    recipient.Name = r.RecipientTitle.Trim() + " " + r.RecipientFirstName.Trim() + " " +
                                     r.RecipientLastName.Trim() + " (" + r.RecipientIdPk.Trim() + ") [" +
                                     relationshipID + "]";
                }
                else
                {
                    recipient.Name = r.RecipientTitle.Trim() + " " + r.RecipientFirstName.Trim() +
                                     " " + r.RecipientLastName.Trim() + " (" + r.RecipientIdPk.Trim() + ")";
                }

                recipient.RecipientID = r.RecipientIdPk.Trim();
                jsonData = JsonConvert.SerializeObject(recipient);
            }
            catch (Exception e)
            {
                recipient = null;
            }

            var jsonResult = Json(new {data = jsonData});
            return jsonResult;
        }

        [HttpGet]
        public IActionResult GetHosptials()
        {
            var jsonData = "";
            var hospitals = new List<TblHospital>();

            try
            {
                hospitals = _config.TblHospitals.ToList();
                
                jsonData = JsonConvert.SerializeObject(hospitals);
            }
            catch (Exception e)
            {
                hospitals = null;
            }

            var jsonResult = Json(new {data = jsonData});
            return jsonResult;
        }

        [HttpGet]
        public IActionResult GetCourtesyTypes()
        {
            var jsonData = "";
            var courtesyTypes = new List<TblCourtesyType>();

            try
            {
                courtesyTypes = _config.TblCourtesyTypes.ToList();

                jsonData = JsonConvert.SerializeObject(courtesyTypes);
            }
            catch (Exception e)
            {
                courtesyTypes = null;
            }

            var jsonResult = Json(new { data = jsonData });
            return jsonResult;
        }

        [HttpGet]
        public IActionResult GetSMTs()
        {
            var jsonData = "";
            var smtList = new List<TblSmt>();

            try
            {
               smtList = _config.TblSmts.ToList();

               jsonData = JsonConvert.SerializeObject(smtList);
            }
            catch (Exception e)
            {
                smtList = null;
            }

            var jsonResult = Json(new { data = jsonData });
            return jsonResult;
        }

        [HttpGet]
        public IActionResult GetRelationshipTypes()
        {
            var jsonData = "";
            var relationshipTypes = new List<TblRelationshipType>();

            try
            {
                relationshipTypes = _config.TblRelationshipTypes.ToList();

                jsonData = JsonConvert.SerializeObject(relationshipTypes);
            }
            catch (Exception e)
            {
                relationshipTypes = null;
            }

            var jsonResult = Json(new { data = jsonData });
            return jsonResult;
        }
    }
}
