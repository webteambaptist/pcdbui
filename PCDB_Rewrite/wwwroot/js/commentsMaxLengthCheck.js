﻿function CommentsMaxLength(text, maxLength) {
    var diff = maxLength - text.value.length;
    if (diff < 0) {
        text.value = text.value.substring(0, maxLength);
        diff = 0;
    }
    $('#lblCharCount')[0].innerText = diff + " characters left";
}