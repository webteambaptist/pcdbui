﻿var courtesyTypeTable;

$(document).ready(function () {
    GenerateCourtesyTypeTable();
});

function GenerateCourtesyTypeTable() {
    courtesyTypeTable = $('#courtesyTypeTable').DataTable({
        "dom": '<"top"l>rt<"bottom"ip><"clear">',
        "destroy": true,
        "bAutoWidth": true,
        "ajax": {
            "url": "../Query/GetCourtesyTypes",
            "error": function (errResp) {
                alert("There was an issue processing your request. Please try again.");
            },
            "dataSrc": function (json) {
                var data = JSON.parse(json.data);
                return data;
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "There are currently no Hospitals."
        },
        "columns": [
            { "data": 'CourtesyType', sDefaultContent: '' },
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'text-center',
            }
        ],
        "aaSorting": [],
        "pageLength": 10,
        "pagingType": "full_numbers",
        responsive: true
    });
}
