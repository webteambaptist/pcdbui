﻿var recipientSearchTable;

var mainUrl;

$(document).ready(function () {
    if (window.location.href.indexOf("pcdb") > -1 || window.location.href.indexOf("PCDB") > -1) {
        mainUrl = '/pcdb/';
    } else {
        mainUrl = '../';
    }

    $('#pcdbTable').hide();
    $("#searchbox").hide();

    var search = sessionStorage.getItem("SearchValue");

    if (search != null) {
        GenerateResults(search);
    }
});

$("#searchbox").keyup(function () {
    recipientSearchTable.rows().search(this.value).draw();
});

function GenerateResults(searchValue) {
    var searchType;

    sessionStorage.setItem("SearchValue", searchValue);

    if (searchValue === "ALL") {
        searchType = "All Names";
    }
    else {
        searchType = "Last Name Letter";
    }

    GenerateInitialList(searchValue, searchType);
}

function GenerateInitialList(searchValue, searchType) {
    recipientSearchTable = $('#pcdbTable').DataTable({
        "dom": '<"top"l>rt<"bottom"ip><"clear">',
        "destroy": true,
        "bAutoWidth": true,
        "ajax": {
            //"url": "../Query/RecipientsSearch",
            "url": mainUrl + "Query/RecipientsSearch",
            "data": {
                "searchValue": searchValue,
                "searchType": searchType
            },
            "error": function(errResp) {
                alert("There was an issue processing your request. Please try again.");
            },
            "dataSrc": function(json) {
                var data = JSON.parse(json.data);
                return data;
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "There are currently no Providers."
        },
        "columns": [
            {
                "data": 'Name',
                sDefaultContent: '',
                "render": function (name, type, data, meta) {

                    if (data.RelationshipID !== null) {
                        var link =  mainUrl + "Home/RecipientFamilyViewEdit?recipientID=" + data.RecipientID;
                        return "<a id='recipientFamilyViewEdit' href='" + link + "'>" + data.Name + "</a>";
                    } else {
                        var link = mainUrl + "Home/RecipientView?recipientID=" + data.RecipientID;
                        return "<a id='recipientView' href='" + link + "'>" + data.Name + "</a>";
                    }
                }
            },
            {
                "data": 'Beaches',
                sDefaultContent: '',
                "render": function(name, type, data, meta) {
                    var link = mainUrl + "Home/CourtesyList?recipientID=" + data.RecipientID;
                    return "<a id='viewCourtestListBeaches' href='" + link + "'>$" + data.Beaches + "</a>";
                }
            },
            {
                "data": 'Downtown',
                sDefaultContent: '',
                "render": function(name, type, data, meta) {
                    var link = mainUrl + "Home/CourtesyList?recipientID=" + data.RecipientID;
                    return "<a id='viewCourtesyListDowntown' href='" + link + "'>$" + data.Downtown + "</a>";
                }
            },
            {
                "data": 'Nassau',
                sDefaultContent: '',
                "render": function(name, type, data, meta) {
                    var link = mainUrl + "Home/CourtesyList?recipientID=" + data.RecipientID;
                    return "<a id='viewCourtesyListNassau' href='" + link + "'>$" + data.Nassau + "</a>";
                }
            },
            {
                "data": 'PHS',
                sDefaultContent: '',
                "render": function(name, type, data, meta) {
                    var link = mainUrl + "Home/CourtesyList?recipientID=" + data.RecipientID;
                    return "<a id='viewCourtesyListPHS' href='" + link + "'>$" + data.PHS + "</a>";
                }
            }
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'text-center',
                'width': '30%'
            },
            {
                targets: 1,
                className: 'text-center'
            },
            {
                targets: 2,
                className: 'text-center'
            },
            {
                targets: 3,
                className: 'text-center'
            },
            {
                targets: 4,
                className: 'text-center'
            }
        ],
        "aaSorting": [],
        "pageLength": 10,
        "pagingType": "full_numbers",
        //"fnPreDrawCallback": function () {
        //    StartLoader();
        //},
        "fnInitComplete": function () {
            //StopLoader();
            $("#searchbox").show();
        },
        responsive: true
    });

    $('#pcdbTable').show();
}
