﻿var recipientSearchTable;
$(document).ready(function () {
    $('#recipientSearchTable').hide();
});

function DisplayRecipientSearch() {
    var modal = document.getElementById("relatedToModal");
    modal.style.display = "block";

    $("#searchbox").hide();
}

function CloseModal() {
    var modal = document.getElementById("relatedToModal");
    modal.style.display = "none";
}

$("#searchbox").keyup(function () {
    recipientSearchTable.rows().search(this.value).draw();
});

function GenerateResults(searchValue) {
    var searchType;
    if (searchValue === "ALL") {
        searchType = "All Names";
    }
    else {
        searchType = "Last Name Letter";
    }

    GeneratePcdbTableByLastName(searchValue, searchType);
}

function GeneratePcdbTableByLastName(searchValue, searchType) {
    recipientSearchTable = $('#recipientSearchTable').DataTable({
        "dom": '<"top"l>rt<"bottom"ip><"clear">',
        "destroy": true,
        "bAutoWidth": true,
        "ajax": {
            "url": "../Query/RecipientsSearchForFamilyRecipients",
            "data": {
                "searchValue": searchValue,
                "searchType": searchType
            },
            "error": function (errResp) {
                alert("There was an issue processing your request. Please try again.");
            },
            "dataSrc": function (json) {
                var data = JSON.parse(json.data);
                return data;
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "There are currently no Providers."
        },
        "columns": [
            {
                "data": 'Name', sDefaultContent: '', "render": function (name, type, data, meta) {
                    var id = data.RecipientID;
                    var link = 'GetProvider(\"' + id + '\")';
                    return "<a id='' href='#' value='" + data.RecipientID + "' onclick='" + link + "'>" + data.Name + "</a>";
                }
            }
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'text-center'
            }
        ],
        "aaSorting": [],
        "pageLength": 10,
        "pagingType": "full_numbers",
        //"fnPreDrawCallback": function () {
        //    StartLoader();
        //},
        "fnInitComplete": function () {
            //StopLoader();
            $("#searchbox").show();
        },
        responsive: true
    });
    $('#recipientSearchTable').show();
}

function GetProvider(recipientId) {
    $.ajax({
        type: "GET",
        url: "../Query/GetProviderName",
        data: {
            "recipientId": recipientId
        },
        success: function (result) {
            var data = JSON.parse(result.data);
            $('#relatedTo').val(data.Name);
            $('#relatedToID').val(data.RecipientID);

            var modal = document.getElementById("relatedToModal");
            modal.style.display = "none";
        }
    });
}