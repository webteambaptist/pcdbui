﻿var relationshipTypeTable;

$(document).ready(function () {
    GenerateRelationshipTypesTable();
});

function GenerateRelationshipTypesTable() {
    relationshipTypeTable = $('#relationshipTypeTable').DataTable({
        "dom": '<"top"l>rt<"bottom"ip><"clear">',
        "destroy": true,
        "bAutoWidth": true,
        "ajax": {
            "url": "../Query/GetRelationshipTypes",
            "error": function (errResp) {
                alert("There was an issue processing your request. Please try again.");
            },
            "dataSrc": function (json) {
                var data = JSON.parse(json.data);
                return data;
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "There are currently no Relationship Types."
        },
        "columns": [
            {
                "data": 'RelationshipTypeIdPk',
                sDefaultContent: '',
                "render": function (name, type, data, meta) {
                    var id = data.RelationshipTypeIdPk;
                    var link = 'UpdateStatus(\"' + id + '\")';
                    if (data.RelationshipType !== "np") {
                        return "<a id='' class='btn btn-info' href='#' value='" + data.RelationshipTypeIdPk + "' onclick='" + link + "'>Toggle Display</a>";   
                    }
                }
            },
            { "data": 'RelationshipType', sDefaultContent: '' },
            { "data": 'DisplayRelationshipType', sDefaultContent: '' },
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'text-center',
            },
            {
                targets: 1,
                className: 'text-center',
            },
            {
                targets: 2,
                className: 'text-center',
            }
        ],
        "aaSorting": [],
        "pageLength": 10,
        "pagingType": "full_numbers",
        responsive: true
    });
}

function UpdateStatus(id) {
    var result = confirm("Are you sure you want to change this Relationship Types status?");

    if (result) {
        $.ajax({
            type: "POST",
            url: "../Submit/ToggleDisplayValue",
            data: {
                "id": id
            },
            success: function (result) {
                location.reload();
            }
        });
    }
}