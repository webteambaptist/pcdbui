﻿
$(document).ready(function () {
    $('#datetimepicker1').datetimepicker();

    var entity = $("#selectedBhEntity").val();
    var g = $("#selectedGivenBy").val();
    var ct = $("#selectedCourtesyType").val();

    $('#bhentity').val(entity).prop('selected', true);
    $('#givenby').val(g).prop('selected', true);
    $('#courtesyType').val(ct).prop('selected', true);
});
