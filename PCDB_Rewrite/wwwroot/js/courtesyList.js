﻿var courtesyListTable;
var recipientId;

$(document).ready(function () {
    var thisYear = new Date().getFullYear();

    $('#year').val(thisYear).prop('selected', true);

    recipientId = $('#recipientId').val();

    sessionStorage.setItem("RecipientId", recipientId);

    GenerateCourtesyListTable(recipientId, thisYear);
});


$("#year").change(function () {
    var selectedYear = $(this).children("option:selected").val();

    GenerateCourtesyListTable(recipientId,selectedYear);
});

function GenerateCourtesyListTable(recipientId, year) {
    courtesyListTable = $('#courtesyListTable').DataTable({
        "dom": '<"top"l>rt<"bottom"ip><"clear">',
        "destroy": true,
        "bAutoWidth": true,
        "ajax": {
            "url": "../Query/CourtesyListSearch",
            "data": {
                "recipientId": recipientId,
                "selectedYear": year
            },
            "error": function(errResp) {
                alert("There was an issue processing your request. Please try again.");
            },
            "dataSrc": function(json) {
                var data = JSON.parse(json.data);
                return data;
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "No records found."
        },
        "columns": [
            {
                "data": 'Date',
                sDefaultContent: '',
                "render": function(name, type, data, meta) {
                    var link = "../Home/CourtesyViewEdit?id=" + data.Id + "&year=" + year;
                    return "<a id='courtesyViewEdit' href='" + link + "'>" + data.Date + "</a>";
                }
            },
            {
                "data": 'BHEntity',
                sDefaultContent: '',
                "render": function(name, type, data, meta) {
                    var link = "../Home/CourtesyViewEdit?id=" + data.Id + "&year=" + year;
                    return "<a id='courtesyViewEdit' target='_blank' href='" + link + "'>" + data.BHEntity + "</a>";
                }
            },
            {
                "data": 'GivenBy',
                sDefaultContent: '',
                "render": function(name, type, data, meta) {
                    var link = "../Home/CourtesyViewEdit?id=" + data.Id + "&year=" + year;
                    return "<a id='courtesyViewEdit' target='_blank' href='" + link + "'>" + data.GivenBy + "</a>";
                }
            },
            {
                "data": 'CourtesyType',
                sDefaultContent: '',
                "render": function(name, type, data, meta) {
                    var link = "../Home/CourtesyViewEdit?id=" + data.Id + "&year=" + year;
                    return "<a id='courtesyViewEdit' target='_blank' href='" + link + "'>" + data.CourtesyType + "</a>";
                }
            },
            {
                "data": 'Value',
                sDefaultContent: '',
                "render": function(name, type, data, meta) {
                    var link = "../Home/CourtesyViewEdit?id=" + data.Id + "&year=" + year;
                    return "<a id='courtesyViewEdit' target='_blank' href='" + link + "'>$" + data.Value + "</a>";
                }
            },
            {
                "data": 'Description',
                sDefaultContent: '',
                "render": function(name, type, data, meta) {
                    var link = "../Home/CourtesyViewEdit?id=" + data.Id + "&year=" + year;
                    return "<a id='courtesyViewEdit' target='_blank' href='" + link + "'>" + data.Description + "</a>";
                }
            }
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'text-center'
            },
            {
                targets: 1,
                className: 'text-center'
            },
            {
                targets: 2,
                className: 'text-center'
            },
            {
                targets: 3,
                className: 'text-center'
            },
            {
                targets: 4,
                className: 'text-center'
            },
            {
                targets: 5,
                className: 'text-center'
            }
        ],
        "aaSorting": [],
        "pageLength": 10,
        "pagingType": "full_numbers",
        responsive: true
    });

    $.ajax({
        type: "GET",
        url: "../Query/GetCourtesyTotals",
        data: {
            "recipientId": recipientId,
            "selectedYear": year
        },
        success: function(result) {
            var data = JSON.parse(result.data);

            $('#beachesTotal').val(data.Beaches);
            $('#downtownTotal').val(data.Downtown);
            $('#nassauTotal').val(data.Nassau);
            $('#phsTotal').val(data.PHS);
        }
    });
}