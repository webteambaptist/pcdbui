﻿
$(document).ready(function () {
    var t = $("#selectedTitle").val();
    var s = $("#selectedSuffix").val();
    var r = $("#relationshipType").val();

    $('#title').val(t).prop('selected', true);
    $('#suffix').val(s).prop('selected', true);
    $('#relationship').val(r).prop('selected', true);
});
