﻿var smtTable;

$(document).ready(function () {
    GenerateSMTTable();
});

function GenerateSMTTable() {
    smtTable = $('#smtTable').DataTable({
        "dom": '<"top"l>rt<"bottom"ip><"clear">',
        "destroy": true,
        "bAutoWidth": true,
        "ajax": {
            "url": "../Query/GetSMTs",
            "error": function (errResp) {
                alert("There was an issue processing your request. Please try again.");
            },
            "dataSrc": function (json) {
                var data = JSON.parse(json.data);
                return data;
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "There are currently no Hospitals."
        },
        "columns": [
            {
                "data": 'SmtidPk',
                sDefaultContent: '',
                "render": function (name, type, data, meta) {
                    var id = data.SmtidPk;
                        var link = 'UpdateStatus(\"' + id + '\")';
                    return "<a id='' class='btn btn-info' href='#' value='" + data.SmtidPk + "' onclick='" + link + "'>Toggle Status</a>";
                }
            },
            {
                "data": '',
                sDefaultContent: '',
                "render": function (name, type, data, meta) {
                    var name;

                    name = data.SmtfirstName + " ";

                    if (data.Smtmi != null) {
                        name += data.Smtmi + " ";
                    }

                    name += data.SmtlastName;

                    return name;
                }
            },
            { "data": 'Smtactive', sDefaultContent: '' },
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'text-center',
            },
            {
                targets: 1,
                className: 'text-center',
            },
            {
                targets: 2,
                className: 'text-center',
            }
        ],
        "aaSorting": [],
        "pageLength": 10,
        "pagingType": "full_numbers",
        responsive: true
    });
}

function UpdateStatus(id) {
    var result = confirm("Are you sure you want to change this user's status?");

    if (result) {
        $.ajax({
            type: "POST",
            url: "../Submit/ToggleSMTStatus",
            data: {
                "id": id
            },
            success: function (result) {
                location.reload();
            }
        });
    }
}