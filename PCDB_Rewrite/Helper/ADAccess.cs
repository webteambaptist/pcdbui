﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Threading.Tasks;

namespace PCDB_Rewrite.Helper
{
    public class ADAccess
    {
        public static bool GetADAccess(string domain, string user, string adGroupName)
        {
            var pc = new PrincipalContext(ContextType.Domain, domain);
            var src = UserPrincipal.FindByIdentity(pc, user).GetGroups(pc);

            var isUserInGroup = src.Select(x => x).Select(x => x.Name).Where(y => y.Contains(adGroupName)).ToList();

            return isUserInGroup.Any();
        }
    }
}
