﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static PCDB_Rewrite.Models.DropsDowns;

namespace PCDB_Rewrite.Models
{
    public class CourtesyTableValues
    {
        public int Id { get; set; }
        public string RecipientId { get; set; }
        public string Date { get; set; }
        public string BHEntity { get; set; }
        public string GivenBy { get; set; }
        public string CourtesyType { get; set; }
        public decimal Value { get; set; }
        public string Description { get; set; }
    }

    public class CourtesyTotal
    {
        public string Beaches { get; set; }
        public string Downtown { get; set; }
        public string Nassau { get; set; }
        public string PHS { get; set; }
    }

    public class CourtesyList
    {
        public string RecipientID { get; set; }
        public string RelatedTo { get; set; }
        public string Name { get; set; }
        public string Beaches { get; set; }
        public string Downtown { get; set; }
        public string Nassau { get; set; }
        public string PHS { get; set; }
    }
    
    public class CourtesyDetails
    {
        public int Id { get; set; }
        public string RecipientId { get; set; }
        public string Recipient { get; set; }
        public string RelatedTo { get; set; }
        public DateTime DateGiven { get; set; }
        public int BHEntity { get; set; }
        public int GivenBy { get; set; }
        public string GivenByStatus { get; set; }
        public string InactiveGivenBy { get; set; }
        public int CourtesyType { get; set; }
        public decimal Value { get; set; }
        public string Description { get; set; }
        public List<GivenByNames> _GivenByNames { get; set; }
        public List<CourtesyTypes> _CourtesyTypes { get; set; }
        public List<BHEntites> _BHEntites { get; set; }
    }

    public class CourtesyAdd
    {
        public string Id { get; set; }
        public string Recipient { get; set; } 
        public string RelatedTo { get; set; }
        public List<BHEntites> _BHEntites { get; set; }
        public List<GivenByNames> _GivenByNames { get; set; }
        public List<CourtesyTypes> _CourtesyTypes { get; set; }
    }
}
