﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PCDB_Rewrite.Models
{
    public class RecipientsSearch
    {
        public string RecipientID { get; set; }
        public string RelationshipID { get; set; }
        public string Name { get; set; }
        public decimal Beaches  { get; set; }
        public decimal Downtown { get; set; }
        public decimal Nassau { get; set; }
        public decimal PHS { get; set; }
    }
    public class FamilyMemberRelatedTo
    {
        public string RecipientID { get; set; }
        public string Name { get; set; }
    }
}
