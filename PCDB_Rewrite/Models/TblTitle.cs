﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PCDB_Rewrite.Models
{
    public partial class TblTitle
    {
        public string Title { get; set; }
    }
}
