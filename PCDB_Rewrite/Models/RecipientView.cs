﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PCDB_Rewrite.Models
{
    public class RecipientView
    {
        public string RecipientID { get; set; }
        public string Name { get; set; }
        public string Comments { get; set; }
    }

    public class RecipientViewFamily
    {
        public string RecipientID { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string RelatedTo { get; set; }
        public string Relationship { get; set; }
        public string Comments { get; set; }
    }
}
