﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PCDB_Rewrite.Models
{
    public class DropsDowns
    {
        public partial class GivenByNames
        {
            public string Id { get; set; }
            public string Name { get; set; }
        }

        public partial class CourtesyTypes
        {
            public string Id { get; set; }
            public string CourtesyType { get; set; }
        }
        public partial class BHEntites
        {
            public string Id { get; set; }
            public string BHEntity { get; set; }
        }

        public partial class Relationships
        {
            public string Id { get; set; }
            public string Relationship { get; set; }
        }
    }
}
