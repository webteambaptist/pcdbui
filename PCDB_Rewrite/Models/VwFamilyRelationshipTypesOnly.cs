﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PCDB_Rewrite.Models
{
    public partial class VwFamilyRelationshipTypesOnly
    {
        public short RelationshipTypeIdPk { get; set; }
        public string RelationshipType { get; set; }
        public string DisplayRelationshipType { get; set; }
    }
}
