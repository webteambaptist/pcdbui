﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace PCDB_Rewrite.Models
{
    public partial class pcdbContext : DbContext
    {
        public pcdbContext()
        {
        }

        public pcdbContext(DbContextOptions<pcdbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblCourtesyType> TblCourtesyTypes { get; set; }
        public virtual DbSet<TblHospital> TblHospitals { get; set; }
        public virtual DbSet<TblImportHistory> TblImportHistories { get; set; }
        public virtual DbSet<TblMedstaffDownloadNotInUse> TblMedstaffDownloadNotInUses { get; set; }
        public virtual DbSet<TblRecipient> TblRecipients { get; set; }
        public virtual DbSet<TblRecipientsAudit> TblRecipientsAudits { get; set; }
        public virtual DbSet<TblRecipientsCourtesiesAudit> TblRecipientsCourtesiesAudits { get; set; }
        public virtual DbSet<TblRecipientsCourtesy> TblRecipientsCourtesies { get; set; }
        public virtual DbSet<TblRelationshipType> TblRelationshipTypes { get; set; }
        public virtual DbSet<TblSmt> TblSmts { get; set; }
        public virtual DbSet<TblSuffix> TblSuffixes { get; set; }
        public virtual DbSet<TblTitle> TblTitles { get; set; }
        public virtual DbSet<TblYear> TblYears { get; set; }
        public virtual DbSet<VwFamilyRelationshipTypesOnly> VwFamilyRelationshipTypesOnlies { get; set; }
        public virtual DbSet<VwRobinThomasHistory> VwRobinThomasHistories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<TblCourtesyType>(entity =>
            {
                entity.HasKey(e => e.CourtesyTypeIdPk)
                    .HasName("PK_tblCourtesies");

                entity.ToTable("tblCourtesyTypes", "dbo");

                entity.Property(e => e.CourtesyTypeIdPk).HasColumnName("CourtesyTypeID_PK");

                entity.Property(e => e.CourtesyType)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblHospital>(entity =>
            {
                entity.HasKey(e => e.HospitalIdPk);

                entity.ToTable("tblHospitals", "dbo");

                entity.Property(e => e.HospitalIdPk).HasColumnName("HospitalID_PK");

                entity.Property(e => e.HospitalName)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblImportHistory>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblImportHistory", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.LastImportDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblMedstaffDownloadNotInUse>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblMedstaffDownload_NOT_IN_USE", "dbo");

                entity.Property(e => e.Dob)
                    .HasColumnType("datetime")
                    .HasColumnName("DOB");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pas)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("PAS#");

                entity.Property(e => e.Title)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Dr')")
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<TblRecipient>(entity =>
            {
                entity.HasKey(e => e.RecipientIdPk);

                entity.ToTable("tblRecipients", "dbo");

                entity.Property(e => e.RecipientIdPk)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("RecipientID_PK");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifiedUser)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Nspflag)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("NSPFlag")
                    .IsFixedLength(true);

                entity.Property(e => e.RecipientComments)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecipientDob)
                    .HasColumnType("datetime")
                    .HasColumnName("RecipientDOB")
                    .HasDefaultValueSql("(null)");

                entity.Property(e => e.RecipientFirstName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RecipientIdFk)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("RecipientID_FK")
                    .HasDefaultValueSql("(0)");

                entity.Property(e => e.RecipientLastName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RecipientMi)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("RecipientMI")
                    .IsFixedLength(true);

                entity.Property(e => e.RecipientSuffix)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.RecipientTitle)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.RecordCreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RelationshipTypeIdFk)
                    .HasColumnName("RelationshipTypeID_FK")
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<TblRecipientsAudit>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblRecipientsAudit", "dbo");

                entity.Property(e => e.AuditTimeStamp)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.AuditType)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Comments)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUser)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Nspflag)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("NSPFlag")
                    .IsFixedLength(true);

                entity.Property(e => e.RecipientComments)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecipientDob)
                    .HasColumnType("datetime")
                    .HasColumnName("RecipientDOB");

                entity.Property(e => e.RecipientFirstName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RecipientIdFk)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("RecipientID_FK");

                entity.Property(e => e.RecipientIdPk)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("RecipientID_PK");

                entity.Property(e => e.RecipientLastName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RecipientMi)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("RecipientMI")
                    .IsFixedLength(true);

                entity.Property(e => e.RecipientSuffix)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.RecipientTitle)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.RecordCreatedDate).HasColumnType("datetime");

                entity.Property(e => e.RelationshipTypeIdFk).HasColumnName("RelationshipTypeID_FK");
            });

            modelBuilder.Entity<TblRecipientsCourtesiesAudit>(entity =>
            {
                entity.HasKey(e => e.AuditIdPk);

                entity.ToTable("tblRecipientsCourtesiesAudit", "dbo");

                entity.Property(e => e.AuditIdPk).HasColumnName("AuditID_PK");

                entity.Property(e => e.AuditTimeStamp)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.AuditType)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Comments)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CourtesyTypeIdFk).HasColumnName("CourtesyTypeID_FK");

                entity.Property(e => e.DateGiven).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.HospitalIdFk).HasColumnName("HospitalID_FK");

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedUser)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.RecipientCourtesyIdPk).HasColumnName("RecipientCourtesyID_PK");

                entity.Property(e => e.RecipientIdFk)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("RecipientID_FK");

                entity.Property(e => e.RecordCreatedDate).HasColumnType("datetime");

                entity.Property(e => e.SmtidFk).HasColumnName("SMTID_FK");

                entity.Property(e => e.ValueGiven).HasColumnType("money");
            });

            modelBuilder.Entity<TblRecipientsCourtesy>(entity =>
            {
                entity.HasKey(e => e.RecipientCourtesyIdPk);

                entity.ToTable("tblRecipientsCourtesies", "dbo");

                entity.Property(e => e.RecipientCourtesyIdPk).HasColumnName("RecipientCourtesyID_PK");

                entity.Property(e => e.CourtesyTypeIdFk).HasColumnName("CourtesyTypeID_FK");

                entity.Property(e => e.DateGiven).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.HospitalIdFk).HasColumnName("HospitalID_FK");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModifiedUser)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.RecipientIdFk)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("RecipientID_FK");

                entity.Property(e => e.RecordCreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SmtidFk).HasColumnName("SMTID_FK");

                entity.Property(e => e.ValueGiven).HasColumnType("money");

                entity.HasOne(d => d.CourtesyTypeIdFkNavigation)
                    .WithMany(p => p.TblRecipientsCourtesies)
                    .HasForeignKey(d => d.CourtesyTypeIdFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tblRecipientsCourtesies_tblCourtesies");

                entity.HasOne(d => d.HospitalIdFkNavigation)
                    .WithMany(p => p.TblRecipientsCourtesies)
                    .HasForeignKey(d => d.HospitalIdFk)
                    .HasConstraintName("FK_tblRecipientsCourtesies_tblHospitals");

                entity.HasOne(d => d.RecipientIdFkNavigation)
                    .WithMany(p => p.TblRecipientsCourtesies)
                    .HasForeignKey(d => d.RecipientIdFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tblRecipientsCourtesies_tblRecipients");
            });

            modelBuilder.Entity<TblRelationshipType>(entity =>
            {
                entity.HasKey(e => e.RelationshipTypeIdPk);

                entity.ToTable("tblRelationshipTypes", "dbo");

                entity.Property(e => e.RelationshipTypeIdPk).HasColumnName("RelationshipTypeID_PK");

                entity.Property(e => e.DisplayRelationshipType)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.RelationshipType)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblSmt>(entity =>
            {
                entity.HasKey(e => e.SmtidPk);

                entity.ToTable("tblSMT", "dbo");

                entity.Property(e => e.SmtidPk).HasColumnName("SMTID_PK");

                entity.Property(e => e.Smtactive)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("SMTActive")
                    .IsFixedLength(true);

                entity.Property(e => e.SmtfirstName)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("SMTFirstName");

                entity.Property(e => e.SmtlastName)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("SMTLastName");

                entity.Property(e => e.Smtmi)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("SMTMI")
                    .IsFixedLength(true);

                entity.Property(e => e.Smtsuffix)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("SMTSuffix");
            });

            modelBuilder.Entity<TblSuffix>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblSuffix", "dbo");

                entity.Property(e => e.Suffix)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<TblTitle>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tblTitles", "dbo");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<TblYear>(entity =>
            {
                entity.HasKey(e => e.Year);

                entity.ToTable("tblYears", "dbo");

                entity.Property(e => e.Year)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.CourtesyLimit).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<VwFamilyRelationshipTypesOnly>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwFamilyRelationshipTypesOnly", "dbo");

                entity.Property(e => e.DisplayRelationshipType)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.RelationshipType)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RelationshipTypeIdPk)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("RelationshipTypeID_PK");
            });

            modelBuilder.Entity<VwRobinThomasHistory>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwRobinThomasHistory", "dbo");

                entity.Property(e => e.BhEntity)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("BH Entity");

                entity.Property(e => e.CourtesyType)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Courtesy Type");

                entity.Property(e => e.DateGiven)
                    .HasColumnType("datetime")
                    .HasColumnName("Date Given");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GivenBy)
                    .HasMaxLength(61)
                    .IsUnicode(false)
                    .HasColumnName("Given By");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("Last Modified Date");

                entity.Property(e => e.LastModifiedUser)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("Last Modified User");

                entity.Property(e => e.RecipientId)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("Recipient ID");

                entity.Property(e => e.RecipientName)
                    .HasMaxLength(61)
                    .IsUnicode(false)
                    .HasColumnName("Recipient Name");

                entity.Property(e => e.Value).HasColumnType("money");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
