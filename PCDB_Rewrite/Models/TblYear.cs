﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PCDB_Rewrite.Models
{
    public partial class TblYear
    {
        public string Year { get; set; }
        public decimal? CourtesyLimit { get; set; }
    }
}
