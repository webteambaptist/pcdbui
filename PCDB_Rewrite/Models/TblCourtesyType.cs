﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PCDB_Rewrite.Models
{
    public partial class TblCourtesyType
    {
        public TblCourtesyType()
        {
            TblRecipientsCourtesies = new HashSet<TblRecipientsCourtesy>();
        }

        public short CourtesyTypeIdPk { get; set; }
        public string CourtesyType { get; set; }

        public virtual ICollection<TblRecipientsCourtesy> TblRecipientsCourtesies { get; set; }
    }
}
